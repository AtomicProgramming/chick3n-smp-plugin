package com.atomicprogramming.SMP;

import com.atomicprogramming.SMP.commands.SpawnCommand;
import com.atomicprogramming.SMP.commands.homeCommand;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class Plugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getLogger().info("SMP Plugin has successfully loaded!");
        getServer().getPluginManager().registerEvents(this, this);
        Objects.requireNonNull(getCommand("spawn")).setExecutor(new SpawnCommand());
        Objects.requireNonNull(getCommand("home")).setExecutor(new homeCommand());
    }

    @Override
    public void onDisable() {
        getLogger().info("SMP Plugin has been successfully unloaded!");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.hasPlayedBefore()) {
            event.setJoinMessage("[" + ChatColor.GREEN + "+" + ChatColor.WHITE + "] " + ChatColor.GREEN + player.getDisplayName() + " has joined the server!");
        } else {
            event.setJoinMessage("[" + ChatColor.GREEN + "+" + ChatColor.WHITE + "] " + ChatColor.GREEN + "Welcome to the SMP " + player.getDisplayName());
        }

    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage("[" + ChatColor.RED + "-" + ChatColor.WHITE + "] " + ChatColor.RED + player.getDisplayName() + " has left the server!");
    }
}
